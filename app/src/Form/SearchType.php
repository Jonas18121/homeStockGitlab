<?php

declare(strict_types=1);

/*
 * This file is part of the Symfony package.
 *
 * (c) Fabien Potencier <fabien@symfony.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Form;

use App\Classe\SearchData;
use App\Entity\Category;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Regex;

class SearchType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('query', TextType::class, [
                'required' => false,
                'attr' => [
                    'placeholder' => 'Recherche ',
                ],
            ])
            ->add('minPriceByMonth', TextType::class, [
                'required' => false,
                'attr' => [
                    'placeholder' => 'Min',
                ],
                'constraints' => [
                    new Regex([
                        'pattern' => '/^[0-9 ]+$/',
                        'message' => 'Veuillez ecrire uniquement des chiffres.',
                    ]),
                ],
            ])
            ->add('maxPriceByMonth', TextType::class, [
                'required' => false,
                'attr' => [
                    'placeholder' => 'Max',
                ],
                'constraints' => [
                    new Regex([
                        'pattern' => '/^[0-9 ]+$/',
                        'message' => 'Veuillez ecrire uniquement des chiffres.',
                    ]),
                ],
            ])
            ->add('minSpace', TextType::class, [
                'required' => false,
                'attr' => [
                    'placeholder' => 'Min',
                ],
                'constraints' => [
                    new Regex([
                        'pattern' => '/^[0-9 ]+$/',
                        'message' => 'Veuillez ecrire uniquement des chiffres.',
                    ]),
                ],
            ])
            ->add('maxSpace', TextType::class, [
                'required' => false,
                'attr' => [
                    'placeholder' => 'Max',
                ],
                'constraints' => [
                    new Regex([
                        'pattern' => '/^[0-9 ]+$/',
                        'message' => 'Veuillez ecrire uniquement des chiffres.',
                    ]),
                ],
            ])
            ->add('categories', EntityType::class, [
                'label' => false,
                'class' => Category::class,
                'required' => false,
                'multiple' => true,
                'expanded' => true,
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => SearchData::class,
            'method' => 'GET',
            'crsf_protection' => false, // enlever la protection, puisque c'est que pour de la recherche
        ]);
    }
}
