<?php

declare(strict_types=1);

/*
 * This file is part of the Symfony package.
 *
 * (c) Fabien Potencier <fabien@symfony.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Repository;

use App\Classe\SearchData;
use App\Entity\StorageSpace;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\QueryBuilder;
use Doctrine\Persistence\ManagerRegistry;
use Knp\Bundle\PaginatorBundle\Pagination\SlidingPagination;
use Knp\Component\Pager\PaginatorInterface;

/**
 * @method StorageSpace|null find($id, $lockMode = null, $lockVersion = null)
 * @method StorageSpace|null findOneBy(array $criteria, array $orderBy = null)
 * @method StorageSpace[]    findAll()
 * @method StorageSpace[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class StorageSpaceRepository extends ServiceEntityRepository
{
    private PaginatorInterface $paginationInterface;

    public const MULTIPLE_100 = 100.00;

    public const NUMBER_ITEM_PER_PAGE = 10;

    public const ONE_FOR_TRUE = 1;

    public const ONLY_INT_AND_SPACE = '/^[0-9 ]+$/';

    public function __construct(
        ManagerRegistry $registry,
        PaginatorInterface $paginationInterface
    ) {
        parent::__construct($registry, StorageSpace::class);

        $this->paginationInterface = $paginationInterface;
    }

    public function findAllStorage(int $page): SlidingPagination
    {
        /** @var array<int, StorageSpace> */
        $data = $this->createQueryBuilder('s')
            ->select('s, b, c')
            ->leftJoin('s.bookings', 'b')
            ->leftJoin('s.category', 'c')
            ->getQuery()
            ->getResult();

        /** @var SlidingPagination */
        $pagination = $this->paginationInterface->paginate($data, $page, self::NUMBER_ITEM_PER_PAGE);

        if ($pagination instanceof SlidingPagination) {
            return $pagination;
        }
    }

    public function findStorageSpaceFromBookingId(int $id): StorageSpace
    {
        /** @var StorageSpace */
        $storageSpace = $this->createQueryBuilder('s')
            ->select('s, b')
            ->leftJoin('s.bookings', 'b')
            ->andWhere('b.id IN (:id)')
            ->setParameter('id', $id)
            ->getQuery()
            ->getSingleResult();

        return $storageSpace;
    }

    public function findOneStorage(int $id): StorageSpace
    {
        /** @var StorageSpace */
        $storageSpace = $this->createQueryBuilder('s')
            ->select('s, b, u, c')
            ->leftJoin('s.bookings', 'b')
            ->leftJoin('s.owner', 'u')
            ->leftJoin('s.category', 'c')
            ->andWhere('s.id IN (:id)')
            ->setParameter('id', $id)
            ->getQuery()
            ->getSingleResult();

        return $storageSpace;
    }

    public function countStorageSpace(): int
    {
        /** @var array<int, int> */
        $count = $this->createQueryBuilder('s')
            ->select('COUNT(s)')
            ->getQuery()
            ->getSingleResult()
        ;

        return (int) $count[1];
    }

    public function findBySearch(SearchData $searchData): SlidingPagination
    {
        /** @var QueryBuilder */
        $query = $this->createQueryBuilder('s')
            ->select('s, b, c')
            ->leftJoin('s.bookings', 'b')
            ->leftJoin('s.category', 'c')
            ->addOrderBy('s.created_at', 'DESC');

        // --------- Search city or ZipCode -------------- //
        $query = $this->toolSearchByCityOrZipCode($searchData, $query);

        // --------- Search Price Min and Max -------------- //
        $query = $this->toolSearchByPrice($searchData, $query);

        // --------- Search Category -------------- //
        $query = $this->toolSearchByCategory($searchData, $query);

        // --------- Search Space -------------- //
        $query = $this->toolSearchBySpace($searchData, $query);

        /** @var array<int, StorageSpace> */
        $data = $query
            ->getQuery()
            ->getResult();

        /** @var SlidingPagination */
        $pagination = $this->paginationInterface->paginate($data, $searchData->getPage(), self::NUMBER_ITEM_PER_PAGE);

        if ($pagination instanceof SlidingPagination) {
            return $pagination;
        }
    }

    // TOOLS

    public function toolSearchByCityOrZipCode(SearchData $searchData, QueryBuilder $query): QueryBuilder
    {
        if (!empty($searchData->getQuery())) {
            $query = $query
                // Si l'user a écrit le code postal d'un produit depuis l'input, on l'affiche
                ->orWhere('s.postalCode LIKE :searchPostalCode')
                ->setParameter('searchPostalCode', "%{$searchData->getQuery()}%") // La recherche est partielle donc,
                // si on ecrit "bon", on va afficher tous les produits qui contiennent "bon"

                // Si l'user a écrit la ville d'un produit depuis l'input, on l'affiche
                ->orWhere('s.city LIKE :searchCity')
                ->setParameter('searchCity', "%{$searchData->getQuery()}%")
            ;
        }

        return $query;
    }

    public function toolSearchByPrice(SearchData $searchData, QueryBuilder $query): QueryBuilder
    {
        /** @var float */
        $minPriceByMonth = $searchData->getMinPriceByMonth().'.00';

        /** @var float */
        $maxPriceByMonth = $searchData->getMaxPriceByMonth().'.00';

        // Si l'user a choisi un prix minimum, on l'affiche
        if (
            !empty($searchData->getMinPriceByMonth())
            && self::ONE_FOR_TRUE === preg_match(self::ONLY_INT_AND_SPACE, (string) $searchData->getMinPriceByMonth(), $matches)
        ) {
            $query = $query
                ->andWhere('s.priceByMonth >= :searchMinPriceByMonth')
                ->setParameter('searchMinPriceByMonth', $minPriceByMonth * self::MULTIPLE_100);
        }

        // Si l'user a choisi un prix maximum, on l'affiche
        if (
            !empty($searchData->getMaxPriceByMonth()
            && self::ONE_FOR_TRUE === preg_match(self::ONLY_INT_AND_SPACE, (string) $searchData->getMaxPriceByMonth(), $matches))
        ) {
            $query = $query
                ->andWhere('s.priceByMonth <= :searchMaxPriceByMonth')
                ->setParameter('searchMaxPriceByMonth', $maxPriceByMonth * self::MULTIPLE_100);
        }

        // Si l'user a choisi un prix minimum et maximum, on l'affiche
        if (
            !empty($searchData->getMinPriceByMonth())
            && self::ONE_FOR_TRUE === preg_match(self::ONLY_INT_AND_SPACE, (string) $searchData->getMinPriceByMonth(), $matches)
            && !empty($searchData->getMaxPriceByMonth())
            && self::ONE_FOR_TRUE === preg_match(self::ONLY_INT_AND_SPACE, (string) $searchData->getMaxPriceByMonth(), $matches)
        ) {
            $query = $query
                ->andWhere('s.priceByMonth >= :searchMinPriceByMonth')
                ->setParameter('searchMinPriceByMonth', $minPriceByMonth * self::MULTIPLE_100)
                ->andWhere('s.priceByMonth <= :searchMaxPriceByMonth')
                ->setParameter('searchMaxPriceByMonth', $maxPriceByMonth * self::MULTIPLE_100);
        }

        return $query;
    }

    public function toolSearchByCategory(SearchData $searchData, QueryBuilder $query): QueryBuilder
    {
        // Si l'user a choisi une ou plusieurs catégorie depuis la checkbox, on l'affiche
        if (!empty($searchData->getCategories())) {
            $query = $query
                ->andWhere('s.category IN (:searchCategories)')
                ->setParameter('searchCategories', $searchData->getCategories());
        }

        return $query;
    }

    public function toolSearchBySpace(SearchData $searchData, QueryBuilder $query): QueryBuilder
    {
        /** @var int */
        $minSpace = $searchData->getMinSpace();

        /** @var int */
        $maxSpace = $searchData->getMaxSpace();

        // Si l'user a choisi un prix minimum, on l'affiche
        if (
            !empty($searchData->getMinSpace())
            && self::ONE_FOR_TRUE === preg_match(self::ONLY_INT_AND_SPACE, (string) $searchData->getMinSpace(), $matches)
        ) {
            $query = $query
                ->andWhere('s.space >= :searchMinSpace')
                ->setParameter('searchMinSpace', $minSpace);
        }

        // Si l'user a choisi un prix maximum, on l'affiche
        if (
            !empty($searchData->getMaxSpace())
            && self::ONE_FOR_TRUE === preg_match(self::ONLY_INT_AND_SPACE, (string) $searchData->getMaxSpace(), $matches)
        ) {
            $query = $query
                ->andWhere('s.space <= :searchMaxSpace')
                ->setParameter('searchMaxSpace', $maxSpace);
        }

        // Si l'user a choisi un prix minimum et maximum, on l'affiche
        if (
            !empty($searchData->getMinSpace())
            && self::ONE_FOR_TRUE === preg_match(self::ONLY_INT_AND_SPACE, (string) $searchData->getMinSpace(), $matches)
            && !empty($searchData->getMaxSpace())
            && self::ONE_FOR_TRUE === preg_match(self::ONLY_INT_AND_SPACE, (string) $searchData->getMaxSpace(), $matches)
        ) {
            $query = $query
                ->andWhere('s.space >= :searchMinSpace')
                ->setParameter('searchMinSpace', $minSpace)
                ->andWhere('s.space <= :searchMaxSpace')
                ->setParameter('searchMaxSpace', $maxSpace);
        }

        return $query;
    }
}
