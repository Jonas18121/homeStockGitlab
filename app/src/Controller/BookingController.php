<?php

declare(strict_types=1);

/*
 * This file is part of the Symfony package.
 *
 * (c) Fabien Potencier <fabien@symfony.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Controller;

use App\Entity\Booking;
use App\Entity\User;
use App\Form\BookingFinishType;
use App\Form\BookingType;
use App\Manager\BookingManager;
use App\Repository\BookingRepository;
use App\Repository\StorageSpaceRepository;
use Doctrine\ORM\EntityManagerInterface;
use Stripe\Stripe;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class BookingController extends AbstractController
{
    /**
     * TODO : A mettre pour un admin.
     *
     * @Route("/booking", name="booking_all")
     */
    public function get_all_booking(BookingRepository $bookingRepository): Response
    {
        return $this->render('pages/booking/list.html.twig', [
            'bookings' => $bookingRepository->findAll(),
        ]);
    }

    /**
     * @Route("/booking/{id}", name="booking_one_for_user", requirements={"id": "\d+"}, methods={"GET"})
     */
    public function get_one_booking_for_user(Booking $booking): Response
    {
        if (!$this->getUser() || $this->getUser() !== $booking->getLodger()) {
            return $this->redirectToRoute('storage_space_all');
        }

        $this->denyAccessUnlessGranted('edit', $booking);

        return $this->render('pages/booking/detail.html.twig', [
            'booking' => $booking,
        ]);
    }

    /**
     * rediriger vers stripe pour le paiement.
     *
     * @Route("/booking/add/storageSpace/{id}", name="booking_add", requirements={"id": "\d+"})
     */
    public function create_booking(
        int $id,
        StorageSpaceRepository $storageSpaceRepository,
        Request $request,
        BookingManager $bookingManager
    ): Response {
        /** @var User|null */
        $user = $this->getUser();

        if (!$user) {
            // Stocker l'URL d'origine dans la session
            $request->getSession()->set('_security.main.target_path', $request->getUri());

            // Redirection vers login
            return $this->redirectToRoute('app_login');
        }

        $booking = new Booking();

        $storageSpace = $storageSpaceRepository->findOneStorage($id);

        $formBooking = $this->createForm(BookingType::class, $booking);

        $formBooking->handleRequest($request);

        if ($formBooking->isSubmitted() && $formBooking->isValid()) {
            return $bookingManager->createdBooking($booking, $storageSpace, $user);
        }

        return $this->render('pages/booking/create.html.twig', [
            'formBooking' => $formBooking->createView(),
            'storageSpace' => $storageSpace,
            'oneBookingTrue' => $bookingManager->verifBookingTrue($user),
        ]);
    }

    /**
     * @Route("/booking/user", name="booking_for_user")
     */
    public function get_all_booking_for_user(
        BookingManager $bookingManager
    ): Response {
        /** @var User|null */
        $user = $this->getUser();

        if (!$user) {
            return $this->redirectToRoute('storage_space_all');
        }

        return $this->render('pages/booking/list_to_user.html.twig', [
            'bookings' => $bookingManager->getAllBookingsForUser($user),
        ]);
    }

    /**
     * TODO : A supprimer.
     *
     * N'est pas utiliser, comme stripe s'occupe de finir une reservation
     *
     * @Route("/booking/form/finish/{id}", name="booking_finish")
     */
    public function get_form_booking_finish(Booking $booking, Request $request, EntityManagerInterface $manager): Response
    {
        if (!$this->getUser()) {
            return $this->redirectToRoute('storage_space_all');
        }

        $this->denyAccessUnlessGranted('show', $booking);

        $formBooking = $this->createForm(BookingFinishType::class, $booking);

        $formBooking->handleRequest($request);

        if ($formBooking->isSubmitted() && $formBooking->isValid()) {
            /* $booking->setCreatedAt(new \DateTime())
                ->setLodger($this->getUser())
            ; */

            $manager->persist($booking);
            $manager->flush();

            return $this->redirectToRoute('storage_space_all');
        }

        return $this->render('pages/booking/finish.html.twig', [
            'formBooking' => $formBooking->createView(),
        ]);
    }

    /**
     * @Route("/booking/delete/{id}", name="booking_delete", requirements={"id": "\d+"})
     */
    public function delete_booking(
        Booking $booking,
        BookingManager $bookingManager,
        Request $request
    ): Response {
        /** @var User|null */
        $user = $this->getUser();

        /** @var string|null */
        $token = $request->get('_token');

        if (!$user || null === $token) {
            return $this->redirectToRoute('storage_space_all');
        }

        $this->denyAccessUnlessGranted('delete', $booking);

        if ($this->isCsrfTokenValid('delete', $token)) {
            $bookingManager->deleteBooking($booking);
        }

        return $this->redirectToRoute('booking_for_user');
    }
}
