<?php

declare(strict_types=1);

/*
 * This file is part of the Symfony package.
 *
 * (c) Fabien Potencier <fabien@symfony.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Controller;

use App\Entity\User;
use App\Form\RegistrationType;
use App\Manager\UserManager;
use App\Repository\UserRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
// use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;

class SecurityController extends AbstractController
{
    /**
     *@Route("/registration", name="app_registration")
     */
    public function register(Request $request, UserManager $userManager, UserPasswordHasherInterface $encoder): Response
    {
        if ($this->getUser()) {
            return $this->redirectToRoute('storage_space_all');
        }

        $user = new User();

        $form = $this->createForm(RegistrationType::class, $user);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            return $userManager->register($user, $encoder);
        }

        return $this->render('pages/security/registration.html.twig', [
            'formRegistration' => $form->createView(),
        ]);
    }

    /**
     * @Route("/login", name="app_login")
     */
    public function login(AuthenticationUtils $authenticationUtils): Response
    {
        if ($this->getUser()) {
            return $this->redirectToRoute('storage_space_all');
        }

        return $this->render('pages/security/login.html.twig', [
            'last_username' => $authenticationUtils->getLastUsername(), // last username entered by the user
            'error' => $authenticationUtils->getLastAuthenticationError(), // get the login error if there is one
        ]);
    }

    /**
     * @Route("/logout", name="app_logout")
     */
    public function logout(): void
    {
        // throw new \LogicException('This method can be blank - it will be intercepted by the logout key on your firewall.');
    }

    // TOOLS

    /**
     *@Route("/registration/email/{email}", name="app_registration_user_exist")
     */
    public function isEmailExist(string $email, UserRepository $userRepository): JsonResponse
    {
        if (null !== $userRepository->isEmailExist($email)) {
            return new JsonResponse(['result' => 'error', 'data' => ['isEmailExist' => true]]);
        }

        return new JsonResponse(['result' => 'success', 'data' => ['isEmailExist' => false]]);
    }
}
