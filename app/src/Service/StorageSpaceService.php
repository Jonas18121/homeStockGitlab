<?php

declare(strict_types=1);

/*
 * This file is part of the Symfony package.
 *
 * (c) Fabien Potencier <fabien@symfony.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Service;

use App\Entity\StorageSpace;
use App\Kernel;
use App\Manager\StorageSpaceManager;
use App\Repository\BookingRepository;
use App\Repository\StorageSpaceRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;

class StorageSpaceService
{
    /**
     * TODO : Fait en 2021 et à améliorer.
     *
     * Lorsque StorageSpaceListener réagi à l'évènnement kernel.request
     * StorageSpaceListener fait fonctionner StorageSpaceService::emitStorageCheckDateEndAt()
     * qui va rendre un espace de stockage disponible si la date de fin de réservation est passé.
     *
     * Si la date d'aujourd'hui est plus grand ou égale à la date de fin de réservation,
     * on met la propriété available de l'entité StorageSpace en true , pour qu'il soit disponible aux autres user
     */
    public function emitStorageCheckDateEndAt(
        Request $request,
        StorageSpaceRepository $storageRepository,
        BookingRepository $bookingRepository,
        EntityManagerInterface $entityManager
    ): void {
        $bookings = $bookingRepository->findAll();

        foreach ($bookings as $key => $booking) {
            $dateCurrent = new \DateTime();

            if ($booking->getDateEndAt()) {
                if ($dateCurrent >= $booking->getDateEndAt()) {
                    /*                              17/03/2021                                 >                                   16/03/2021        retourne +1 jour
                        soit la date du jour ->diff(new \DateTime()) a au minimun 1 jour de plus que la date en comparaison $booking->getDateEndAt()

                                                    17/03/2021                                <                                    18/03/2021         retourne -1 jour
                        soit la date du jour ->diff(new \DateTime()) a au minimun 1 jour de moins que la date en comparaison $booking->getDateEndAt()

                                                    17/03/2021                         ==                    17/03/2021          retourne +0 jour
                        soit la date du jour ->diff(new \DateTime()) est égale à la date en comparaison $booking->getDateEndAt()
                    */
                    $nb_days_positif_or_negatif = $booking->getDateEndAt()->diff(new \DateTime());

                    $nb_days = $nb_days_positif_or_negatif->format('%R%a'); // exemple retourne +1 ou -1 ou +0

                    if (0 === (int) $nb_days && false === $booking->getFinish()) {
                        $storageSpace = $storageRepository->findStorageSpaceFromBookingId($booking->getId());

                        $storageSpace->setAvailable(true);
                        $entityManager->persist($storageSpace);

                        $booking->setFinish(true);
                        $entityManager->persist($booking);
                    }
                }
            }
        }

        $entityManager->flush();
    }

    /**
     * TODO : Fait en 2021 et à remplacer CalculPriceByMonth du manager.
     *
     * calcule le prix par mois de chaque espace de stockage
     * après chaque création ou chaque modification,
     * que ce soit dans EasyAdmin ou dans l'interface normale.
     */
    public function emitStorageCalculPriceByMonth(
        Request $request,
        StorageSpaceRepository $storageRepository,
        EntityManagerInterface $entityManager,
        StorageSpaceManager $storageSpaceManager
    ): void {
        $storageSpaces = $storageRepository->findAll();

        foreach ($storageSpaces as $storageSpace) {
            $priceByMonth = $storageSpaceManager->priceByMonth($storageSpace);

            if (null === $storageSpace->getPriceByMonth() || $priceByMonth !== $storageSpace->getPriceByMonth()) {
                $storageSpace->setPriceByMonth($priceByMonth);
                $entityManager->persist($storageSpace);
            }
        }

        $entityManager->flush();
    }
}
