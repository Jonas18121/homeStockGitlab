<?php

declare(strict_types=1);

/*
 * This file is part of the Symfony package.
 *
 * (c) Fabien Potencier <fabien@symfony.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Service;

use App\Repository\BookingRepository;
use App\Repository\StorageSpaceRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;

class BookingService
{
    /**
     * TODO : Fait en 2021 et à améliorer.
     *
     * Si le payement est ok, le storage devient indisponible
     * et on confirme que le check de payement pour la réservation a été fait.
     *
     * $storageSpace->setAvailable(false);
     * $booking->setCheckForPayement(true);
     */
    public function emitBookingPaymentOk(
        Request $response,
        BookingRepository $bookingRepository,
        StorageSpaceRepository $storageRepository,
        EntityManagerInterface $entityManager
    ): void {
        $bookings = $bookingRepository->findAll();

        foreach ($bookings as $key => $booking) {
            if (
                true === $booking->getPay()
                && false === $booking->getFinish()
                && false === $booking->getCheckForPayement()
            ) {
                $storageSpace = $storageRepository->findStorageSpaceFromBookingId($booking->getId());

                $storageSpace->setAvailable(false);
                $entityManager->persist($storageSpace);

                $booking->setCheckForPayement(true);
                $entityManager->persist($booking);
            }
        }

        $entityManager->flush();
    }
}
