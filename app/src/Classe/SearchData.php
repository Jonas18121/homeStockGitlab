<?php

declare(strict_types=1);

/*
 * This file is part of the Symfony package.
 *
 * (c) Fabien Potencier <fabien@symfony.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Classe;

use App\Entity\Category;

class SearchData
{
    private int $page = 1;

    private ?string $query = '';

    private ?string $minPriceByMonth = '';

    private ?string $maxPriceByMonth = '';

    private ?string $minSpace = '';

    private ?string $maxSpace = '';

    /** @var Category[]|null */
    private $categories = [];

    public function getQuery(): ?string
    {
        return $this->query;
    }

    public function setQuery(?string $query): self
    {
        $this->query = $query;

        return $this;
    }

    public function getPage(): int
    {
        return $this->page;
    }

    public function setPage(int $page): self
    {
        $this->page = $page;

        return $this;
    }

    public function getMinPriceByMonth(): ?string
    {
        return $this->minPriceByMonth;
    }

    public function setMinPriceByMonth(?string $minPriceByMonth): self
    {
        $this->minPriceByMonth = $minPriceByMonth;

        return $this;
    }

    public function getMaxPriceByMonth(): ?string
    {
        return $this->maxPriceByMonth;
    }

    public function setMaxPriceByMonth(?string $maxPriceByMonth): self
    {
        $this->maxPriceByMonth = $maxPriceByMonth;

        return $this;
    }

    public function getMinSpace(): ?string
    {
        return $this->minSpace;
    }

    public function setMinSpace(?string $minSpace): self
    {
        $this->minSpace = $minSpace;

        return $this;
    }

    public function getMaxSpace(): ?string
    {
        return $this->maxSpace;
    }

    public function setMaxSpace(?string $maxSpace): self
    {
        $this->maxSpace = $maxSpace;

        return $this;
    }

    /**
     * @return Category[]|null
     */
    public function getCategories(): ?array
    {
        return $this->categories;
    }

    /**
     * @param Category[]|null $categories
     */
    public function setCategories($categories): self
    {
        $this->categories = $categories;

        return $this;
    }
}
