import { RegisterCheckFunction } from './register-check-function';

$(async function () {
    const registerCheckFunction = new RegisterCheckFunction();

    // Last name
    registerCheckFunction.checkLastName();

    // First name
    registerCheckFunction.checkFirstName();

    // Email
    registerCheckFunction.checkEmailWhileWriteIntoInput();
    await registerCheckFunction.checkEmailAfterSubmit();

    // Password
    registerCheckFunction.checkIfSamePasswordWhileWriteIntoInput();
    registerCheckFunction.checkIfSameComfirmPasswordWhileWriteIntoInput();
});
