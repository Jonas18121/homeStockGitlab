import { FormCheckFunction } from '../../../component/form/form-check-function';

export class RegisterCheckFunction {

    constructor() {
        this.formCheckFunction = new FormCheckFunction();
        this.colorRed = '#dc3545';
        this.colorGreen = '#28a745';
        this.colorOrange = '#D07B21';
    }

    /**
     * Vérifier Le prénom
     * 
     * @returns {void}
     */
    checkFirstName() {
        // Cible RegisterCheckFunction car $(document) avec function (event) va recréer un nouveau this
        let self = this; 

        $(document).on('input change focusout', '#registration_firstName', function (event) {
            self.formCheckFunction.validInputLength(
                this,
                "#error_firstName",
                'Ce champ ne doit pas être vide',
                "Le prénom est trop court",
                "Trop de caractère",
                ""
            );
        });
    }

    /**
     * Vérifier Le prénom
     * 
     * @returns {void}
     */
    checkLastName() {
        // Cible RegisterCheckFunction car $(document) avec function (event) va recréer un nouveau this
        let self = this; 

        $(document).on('input change focusout', '#registration_lastName', function (event) {            
            self.formCheckFunction.validInputLength(
                this,
                "#error_lastName",
                'Ce champ ne doit pas être vide',
                "Le nom est trop court",
                "Trop de caractère",
                ""
            );
        });
    }

    /**
     * Vérifier si le mot de passe de comfirmation est égale au premier mot de passe 
     * pendant que l'user écrit dans l'input
     * 
     * @returns {void}
     */
    checkIfSamePasswordWhileWriteIntoInput() {
        // Cible RegisterCheckFunction car $(document) avec function (event) va recréer un nouveau this
        let self = this; 

        $(document).on('input change focusout', '#registration_password', function (event) {
            self.formCheckFunction.samePassword(
                this,
                "#registration_confirm_password",
                "#error_confirm_password",
                '',
                "Les deux passwords sont identique",
                "Les deux passwords ne sont pas identique",
                "Les deux passwords ne sont pas identique, il y a un caractère qui ne correspond pas ou il y a trop ou pas assé de caractères",
                self.colorGreen,
                self.colorGreen,
                self.colorRed,
                self.colorRed
            );
        });
    }

    /**
     * Vérifier si le mot de passe de comfirmation est égale au premier mot de passe 
     * pendant que l'user écrit dans l'input
     * 
     * @returns {void}
     */
    checkIfSameComfirmPasswordWhileWriteIntoInput() {
        // Cible RegisterCheckFunction car $(document) avec function (event) va recréer un nouveau this
        let self = this; 

        $(document).on('input change focusout', '#registration_confirm_password', function (event) {
            self.formCheckFunction.sameComfirmPassword(
                this,
                "#registration_password",
                "#error_confirm_password",
                '',
                "Les deux passwords sont identique",
                "Les deux passwords ne sont pas identique",
                "Les deux passwords ne sont pas identique, il y a un caractère qui ne correspond pas",
                self.colorGreen,
                self.colorGreen,
                self.colorRed,
                self.colorRed
            );
        });
    }

    /**
     * Vérifier si l'email dans le champ est valide pendant que l'user écrit dans l'input
     * 
     * @returns {void}
     */
    checkEmailWhileWriteIntoInput() {
        // Cible RegisterCheckFunction car $(document) avec function (event) va recréer un nouveau this
        let self = this; 

        $(document).on('input', async function (event) {
            self.formCheckFunction.isEmailExist(
                '#registration_email', 
                '/registration/email/', 
                '#error_email',
                'Cette adresse email est déjà utilisé.', 
                self.colorOrange, 
                self.colorOrange
            );
        });
    }

    /**
     * vérifier si l'email dans le champ est valide après le submit du formulaire
     * 
     * @returns {void|boolean}
     */
    checkEmailAfterSubmit() {
        // Cible RegisterCheckFunction car $(document) avec function (event) va recréer un nouveau this
        let self = this; 

        $(document).on('submit', '#user-registration-form', async function (event) {
            const formRegistration = $('#user-registration-form');
            
            self.formCheckFunction.stopEventAndBackToTop(event);

            // Supprimer le gestionnaire d'événement sumbit pour éviter une récursion infinie
            $(document).off('submit', '#user-registration-form');

            let data = [];
            await data.push(
                await self.formCheckFunction.isEmailExist(
                    '#registration_email', 
                    '/registration/email/', 
                    '#error_email',
                    'Cette email est déjà utilisé.', 
                    self.colorRed, 
                    self.colorOrange
                ) 
            );

            let isValid = self.formCheckFunction.isValidField(data);
            self.formCheckFunction.checkAfterSubmitAsync(isValid, event, formRegistration);
        });
    }
}