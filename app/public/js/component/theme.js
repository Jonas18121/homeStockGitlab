import '../../css/component/theme/index.css';

// Changer le theme du site
$(function () {
    const TRUE = true;
    const FALSE = false;
    const THEME_DARK = 'dark';
    const toggleButton = document.querySelector('#switch input');
    const tagBody = document.body;
    const saveTheme = localStorage.theme;

    // Au chargement de la page mettre en mode sombre si c'est sélectionné
    if (THEME_DARK === saveTheme) {
        tagBody.classList.add(saveTheme);
        toggleButton.checked = TRUE;
    }

    toggleButton.addEventListener("click", function(event) {
        tagBody.classList.toggle(THEME_DARK);
        toggleButton.checked = FALSE;
        let themeInProgress = '';

        if (TRUE === tagBody.classList.contains(THEME_DARK)) {
            themeInProgress =  THEME_DARK ;
            toggleButton.checked = TRUE;
        }

        localStorage.theme = themeInProgress;
    });
});

// Synchronisé theme du Système du PC et le theme du site
$(function () {
    const TRUE = true;
    const FALSE = false;
    const THEME_DARK = 'dark';
    const tagBody = document.body;
    const toggleButton = document.querySelector('#switch input');

    window.matchMedia("(prefers-color-scheme:" + THEME_DARK + ")")
        .addEventListener("change", (event) => {
            tagBody.classList.remove(THEME_DARK);
            localStorage.theme = '';
            toggleButton.checked = FALSE;

            if (event.matches) {
                tagBody.classList.add(THEME_DARK);
                localStorage.theme = THEME_DARK;
                toggleButton.checked = TRUE;
            }
        })
    ;
});