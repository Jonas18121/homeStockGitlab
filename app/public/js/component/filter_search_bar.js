import '../../css/component/filter_search_bar/index.css';

$(function () {
    $('.btn_filter p .container_word').on('click', function () {
        // Sélectionner la balise <i> existante
        let existingIcon = $('.btn_filter p .container_word i');

        // Vérifier si la classe 'fas fa-sort-amount-down-alt' existe dans la balise <i>
        let isDownIcon = existingIcon.hasClass('fas fa-sort-amount-up-alt');

        // Sélectionner la balise <span> existante
        let existingSpan = $('.btn_filter p .container_word span');

        if (isDownIcon && existingSpan.text() === "Plus de filtres") {
            // Changer le texte
            existingSpan.text("Moins de filtres");

            // Si la classe 'fas fa-sort-amount-down-alt' est présente, supprimer là
            // et ajouter fas fa-sort-amount-up-alt
            existingIcon.removeClass();
            existingIcon.addClass("fas fa-sort-amount-down-alt");

        } else {
            // Changer le texte
            existingSpan.text("Plus de filtres");

            // Sinon, supprimer fas fa-sort-amount-up-alt
            // et ajouter fas fa-sort-amount-down-alt
            existingIcon.removeClass();
            existingIcon.addClass("fas fa-sort-amount-up-alt");
        }

		$(".search_container_filter").toggle();
	});
});