<?php

declare(strict_types=1);

/*
 * This file is part of the Symfony package.
 *
 * (c) Fabien Potencier <fabien@symfony.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Tests\Entity;

use App\Entity\Comment;
use App\Entity\StorageSpace;
use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\Validator\Validator\TraceableValidator;

/**
 * @see \Doctrine\DBAL\Driver\Middleware
 *
 * https://phpunit.readthedocs.io/fr/latest/textui.html.
 *
 * php bin/phpunit tests/Entity/CommentValidationTest.php
 */
class CommentValidationTest extends KernelTestCase
{
    public function testEntityIsValid(): void
    {
        self::bootKernel();

        $comment = $this->getEntity();

        // utiliser le container validator pour vérifier les règles qu'on a mis sur l'entité.
        // Les error seront contenu dans $errors
        /** @var TraceableValidator */
        $validator = self::$container->get('validator');
        $errors = $validator->validate($comment);

        self::assertCount(0, $errors);
    }

    /**
     * Doit retourner 1 erreur car dans l'entité Comment
     * On test NotBlank.
     */
    public function testContentIsInvalidBlank(): void
    {
        self::bootKernel();

        $comment = $this->getEntity();
        $comment->setContent('');

        /** @var TraceableValidator */
        $validator = self::$container->get('validator');
        $errors = $validator->validate($comment);

        self::assertCount(1, $errors);
    }

    public function testReplyToCommentGetEach(): void
    {
        $comment = $this->userAddRepliesToComment();

        foreach ($comment->getReplies() as $key => $value) {
            self::assertTrue($comment->getReplies()->contains($value));
            $comment->removeReply($value);
        }
    }

    // TOOLS

    public function getEntity(): Comment
    {
        $comment = new Comment();
        $storageSpace = new StorageSpace();
        $owner = new User();
        $parent = new Comment();

        return $comment
            ->setContent('New content to comment')
            ->setStorageSpace($storageSpace)
            ->setOwner($owner)
            ->setParent($parent)
        ;
    }

    public function userAddRepliesToComment(): Comment
    {
        self::bootKernel();

        $comment = $this->getEntity();
        $storageSpace = new StorageSpace();
        $storageSpace->addComment($comment);

        // Obtenir le premier user de la BDD de test
        // $user = static::getContainer()->get('doctrine.orm.entity_manager')->find(User::class, 1);
        /** @var EntityManagerInterface */
        $entityManager = self::$container->get('doctrine.orm.entity_manager');
        $user = $entityManager->find(User::class, 1);

        $replies = [];
        for ($i = 0; $i <= 5; ++$i) {
            $reply = new Comment();
            $reply->setContent("New replies {$i}")
                ->setOwner($user)
                ->setStorageSpace($storageSpace)
                ->setCreatedAt(new \DateTime('now'))
                ->setParent($comment)
            ;

            $comment->addReply($reply);
        }

        return $comment;
    }
}
