<?php

declare(strict_types=1);

/*
 * This file is part of the Symfony package.
 *
 * (c) Fabien Potencier <fabien@symfony.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Tests\Entity;

use App\Entity\Category;
use App\Entity\StorageSpace;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\Validator\Validator\TraceableValidator;

/**
 * @see \Doctrine\DBAL\Logging\Middleware
 *
 * https://phpunit.readthedocs.io/fr/latest/textui.html.
 *
 * php bin/phpunit tests/Entity/CategoryValidationTest.php
 */
class CategoryValidationTest extends KernelTestCase
{
    /**
     * @see \Doctrine\DBAL\Driver\Middleware
     */
    public function testEntityIsValid(): void
    {
        self::bootKernel();

        $category = $this->getEntity();

        // utiliser le container validator pour vérifier les règles qu'on a mis sur l'entité.
        // Les error seront contenu dans $errors
        /** @var TraceableValidator */
        $validator = self::$container->get('validator');
        $errors = $validator->validate($category);

        self::assertCount(0, $errors);
    }

    /**
     * Doit retourner 2 erreurs car dans l'entité Category
     * On test NotBlank et la longueur Length.
     */
    public function testNameIsInvalidBlank(): void
    {
        self::bootKernel();

        $category = $this->getEntity();
        $category->setName('');

        /** @var TraceableValidator */
        $validator = self::$container->get('validator');
        $errors = $validator->validate($category);

        self::assertCount(2, $errors);
    }

    /**
     * Doit retourner 1 erreurs car dans l'entité Category
     * On test uniquement la longueur Length.
     */
    public function testNameIsInvalidLength(): void
    {
        self::bootKernel();

        $category = $this->getEntity();
        $category->setName('P');

        /** @var TraceableValidator */
        $validator = self::$container->get('validator');
        $errors = $validator->validate($category);

        self::assertCount(1, $errors);
    }

    // TOOLS

    public function getEntity(): Category
    {
        $category = new Category();
        $storageSpace = new StorageSpace();

        return $category
            ->setName('New category')
            ->addStorageSpace($storageSpace)
        ;
    }
}
