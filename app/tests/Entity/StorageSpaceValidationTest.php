<?php

declare(strict_types=1);

/*
 * This file is part of the Symfony package.
 *
 * (c) Fabien Potencier <fabien@symfony.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Tests\Entity;

use App\Entity\Category;
use App\Entity\Comment;
use App\Entity\StorageSpace;
use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\Validator\Validator\TraceableValidator;

/**
 * @see \Doctrine\DBAL\Driver\Middleware
 *
 * https://phpunit.readthedocs.io/fr/latest/textui.html.
 *
 * php bin/phpunit tests/Entity/StorageSpaceValidationTest.php
 */
class StorageSpaceValidationTest extends KernelTestCase
{
    public function testEntityIsValid(): void
    {
        self::bootKernel();

        $storageSpace = $this->getEntity();

        // utiliser le container validator pour vérifier les règles qu'on a mis sur l'entité.
        // Les error seront contenu dans $errors
        /** @var TraceableValidator */
        $validator = self::$container->get('validator');
        $errors = $validator->validate($storageSpace);

        self::assertCount(0, $errors);
    }

    /**
     * Doit retourner 2 erreurs car dans l'entité StorageSpace
     * On test NotBlank et la longueur Length.
     */
    public function testTitleIsInvalidBlank(): void
    {
        self::bootKernel();

        $storageSpace = $this->getEntity();
        $storageSpace->setTitle('');

        /** @var TraceableValidator */
        $validator = self::$container->get('validator');
        $errors = $validator->validate($storageSpace);

        self::assertCount(2, $errors);
    }

    /**
     * Doit retourner 1 erreurs car dans l'entité StorageSpace
     * On test uniquement la longueur Length.
     */
    public function testTitleIsInvalidLength(): void
    {
        self::bootKernel();

        $storageSpace = $this->getEntity();
        $storageSpace->setTitle('P');

        /** @var TraceableValidator */
        $validator = self::$container->get('validator');
        $errors = $validator->validate($storageSpace);

        self::assertCount(1, $errors);
    }

    /**
     * Doit retourner 1 erreur car dans l'entité StorageSpace
     * On test NotBlank.
     */
    public function testDescriptionIsInvalidBlank(): void
    {
        self::bootKernel();

        $storageSpace = $this->getEntity();
        $storageSpace->setDescription('');

        /** @var TraceableValidator */
        $validator = self::$container->get('validator');
        $errors = $validator->validate($storageSpace);

        self::assertCount(1, $errors);
    }

    public function testDescriptionIsInvalidText(): void
    {
        self::bootKernel();

        $storageSpace = $this->getEntity();
        $storageSpace->setDescription('Hello <p>world</p>');

        /** @var TraceableValidator */
        $validator = self::$container->get('validator');
        $errors = $validator->validate($storageSpace);

        self::assertCount(1, $errors);
    }

    public function testAdressIsInvalidBlank(): void
    {
        self::bootKernel();

        $storageSpace = $this->getEntity();
        $storageSpace->setAdresse('');

        /** @var TraceableValidator */
        $validator = self::$container->get('validator');
        $errors = $validator->validate($storageSpace);

        self::assertCount(1, $errors);
    }

    // exclude "&~"'{-|`_\^@)]°=+}<>:;!"
    public function testAdressIsInvalidText(): void
    {
        self::bootKernel();

        $storageSpace = $this->getEntity();
        $storageSpace->setAdresse('123 @');

        /** @var TraceableValidator */
        $validator = self::$container->get('validator');
        $errors = $validator->validate($storageSpace);

        self::assertCount(1, $errors);
    }

    public function testCityIsInvalidBlank(): void
    {
        self::bootKernel();

        $storageSpace = $this->getEntity();
        $storageSpace->setCity('');

        /** @var TraceableValidator */
        $validator = self::$container->get('validator');
        $errors = $validator->validate($storageSpace);

        self::assertCount(1, $errors);
    }

    public function testCityIsInvalidTextLeftDash(): void
    {
        self::bootKernel();

        $storageSpace = $this->getEntity();
        $storageSpace->setCity('-ville');

        /** @var TraceableValidator */
        $validator = self::$container->get('validator');
        $errors = $validator->validate($storageSpace);

        self::assertCount(1, $errors);
    }

    public function testCityIsInValidTextRightDash(): void
    {
        self::bootKernel();

        $storageSpace = $this->getEntity();
        $storageSpace->setCity('ville-');

        /** @var TraceableValidator */
        $validator = self::$container->get('validator');
        $errors = $validator->validate($storageSpace);

        self::assertCount(1, $errors);
    }

    /**
     * Doit retourner 2 erreurs car dans l'entité StorageSpace
     * On test NotBlank et Length.
     */
    public function testPostalCodeIsInvalidBlank(): void
    {
        self::bootKernel();

        $storageSpace = $this->getEntity();
        $storageSpace->setPostalCode('');

        /** @var TraceableValidator */
        $validator = self::$container->get('validator');
        $errors = $validator->validate($storageSpace);

        self::assertCount(2, $errors);
    }

    /**
     * Doit retourner 2 erreurs car dans l'entité StorageSpace
     * On test Length et Regex pour avoir uniquement des nombres.
     */
    public function testPostalCodeIsInvalidTextNotNumber(): void
    {
        self::bootKernel();

        $storageSpace = $this->getEntity();
        $storageSpace->setPostalCode('Code postal');

        /** @var TraceableValidator */
        $validator = self::$container->get('validator');
        $errors = $validator->validate($storageSpace);

        self::assertCount(2, $errors);
    }

    /**
     * Doit retourner 1 erreur car dans l'entité StorageSpace
     * On test Regex pour avoir uniquement 5 nombres.
     */
    public function testPostalCodeIsInvalidTextNotFiveNumber(): void
    {
        self::bootKernel();

        $storageSpace = $this->getEntity();
        $storageSpace->setPostalCode('4444');

        /** @var TraceableValidator */
        $validator = self::$container->get('validator');
        $errors = $validator->validate($storageSpace);

        self::assertCount(1, $errors);
    }

    public function testCommentCount(): void
    {
        $storageSpace = $this->userAddComment();

        self::assertCount(5, $storageSpace->getComments());
    }

    public function testCommentGetEach(): void
    {
        $storageSpace = $this->userAddComment();

        foreach ($storageSpace->getComments() as $key => $value) {
            self::assertTrue($storageSpace->getComments()->contains($value));
            $storageSpace->removeComment($value);
        }
    }

    // TOOLS

    public function getEntity(): StorageSpace
    {
        $user = new User();
        $category = new Category();
        $storageSpace = new StorageSpace();

        return $storageSpace
            ->setTitle('Hangar à louer')
            ->setDescription('Spacieux Hangar à louer')
            ->setAdresse('123, rue du clochétier-dôme ')
            ->setPostalCode('44000')
            ->setCity('Nantes-test')
            ->setAvailable(true)
            ->setOwner($user)
            ->setSpace(20)
            ->setPriceByDays(1.00)
            ->setPriceByMonth(30.00)
            ->setCategory($category)
            ->setImages('myImage.jpg')
            ->setCreatedAt(new \DateTime('now'))
        ;
    }

    public function userAddComment(): StorageSpace
    {
        self::bootKernel();

        $storageSpace = $this->getEntity();

        // Obtenir le premier user de la BDD de test
        // $user = static::getContainer()->get('doctrine.orm.entity_manager')->find(User::class, 1);
        /** @var EntityManagerInterface */
        $entityManager = self::$container->get('doctrine.orm.entity_manager');
        $user = $entityManager->find(User::class, 1);

        for ($i = 0; $i < 5; ++$i) {
            $comment = new Comment();
            $comment->setContent("New comment {$i}")
                ->setOwner($user)
                ->setStorageSpace($storageSpace)
                ->setCreatedAt(new \DateTime('now'))
            ;

            $storageSpace->addComment($comment);
        }

        return $storageSpace;
    }
}
