<?php

declare(strict_types=1);

/*
 * This file is part of the Symfony package.
 *
 * (c) Fabien Potencier <fabien@symfony.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Tests\Func;

use App\Entity\StorageSpace;
use Symfony\Bundle\FrameworkBundle\KernelBrowser;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

// tester les controlleurs et l'application en générale

/**
 * https://phpunit.readthedocs.io/fr/latest/textui.html.
 *
 * php bin/phpunit tests/Func/UserTest.php
 */
class StorageSpaceTest extends AbstractEndPoint
{
    private KernelBrowser $client;

    protected function setUp(): void
    {
        parent::setUp();
        $this->client = self::createClient();
    }

    /**
     * Accéder à la page de liste de StorageSpace sans être connecter.
     */
    public function testGetListStorageSpaceUserAnonymous(): void
    {
        /** @var ContainerInterface */
        $containerInterface = $this->client->getContainer();
        /** @var UrlGeneratorInterface */
        $urlGeneratorInterface = $containerInterface->get('router');

        $crawler = $this->client->request(
            Request::METHOD_GET,
            $urlGeneratorInterface->generate('storage_space_all')
        );

        self::assertResponseIsSuccessful();
        self::assertSame(Response::HTTP_OK, $this->client->getResponse()->getStatusCode());

        // récupérer les StorageSpace de la page 1
        $storageSpaces = $crawler->filter('article.card');
        self::assertCount(10, $storageSpaces);
    }

    public function testClickOnPaginationFromListStorageSpaceUserAnonymous(): void
    {
        /** @var ContainerInterface */
        $containerInterface = $this->client->getContainer();
        /** @var UrlGeneratorInterface */
        $urlGeneratorInterface = $containerInterface->get('router');

        $crawler = $this->client->request(
            Request::METHOD_GET,
            $urlGeneratorInterface->generate('storage_space_all') // '/storageSpace'
        );

        self::assertResponseIsSuccessful();
        self::assertSame(Response::HTTP_OK, $this->client->getResponse()->getStatusCode());

        // récupérer les StorageSpace de la page
        $storageSpaces = $crawler->filter('article.card');
        self::assertCount(10, $storageSpaces);

        // via selectLink on selectionne le button de pagination 2
        // extract permet d'avoir le lien pour la page 2
        $link = $crawler->selectLink('2')->extract(['href'])[0];

        // On va sur la page 2
        $crawler = $this->client->request(Request::METHOD_GET, $link);

        self::assertResponseIsSuccessful();
        self::assertSame(Response::HTTP_OK, $this->client->getResponse()->getStatusCode());

        // récupérer les StorageSpace de la page 2
        $storageSpaces = $crawler->filter('article.card');
        self::assertCount(10, $storageSpaces);
    }
}
