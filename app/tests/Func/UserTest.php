<?php

declare(strict_types=1);

/*
 * This file is part of the Symfony package.
 *
 * (c) Fabien Potencier <fabien@symfony.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Tests\Func;

use App\DataFixtures\AppFixtures;
use App\Entity\StorageSpace;
use App\Entity\User;
use Faker\Factory;
use Symfony\Bundle\FrameworkBundle\KernelBrowser;
use Symfony\Component\BrowserKit\Cookie;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Session\SessionFactory;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
// use Symfony\Component\Security\Csrf\TokenStorage\SessionTokenStorage;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;
use Symfony\Component\Security\Csrf\CsrfTokenManager;

// tester les controlleurs et l'application en générale

/**
 * https://phpunit.readthedocs.io/fr/latest/textui.html.
 *
 * php bin/phpunit tests/Func/UserTest.php
 */
class UserTest extends AbstractEndPoint
{
    private KernelBrowser $client;
    private string $userPayload = '{"email": "%s", "password": "password"}';
    // private SessionInterface $session;

    protected function setUp(): void
    {
        parent::setUp();
        $this->client = static::createClient();
    }

    /**
     * Accéder à la page d'accueil sans être connecter.
     */
    public function testGetHomeUserAnonymous(): void
    {
        /** @var ContainerInterface */
        $containerInterface = $this->client->getContainer();
        /** @var UrlGeneratorInterface */
        $urlGeneratorInterface = $containerInterface->get('router');

        $crawler = $this->client->request(
            Request::METHOD_GET,
            $urlGeneratorInterface->generate('home_page')
        );

        self::assertResponseIsSuccessful();
        self::assertSame(Response::HTTP_OK, $this->client->getResponse()->getStatusCode());

        self::assertSelectorExists('.page_home_div_french_button');
        self::assertSelectorTextContains('.page_home_div_french_button', 'Chercher un espace de stockage'); // check si ce texte est dans button.page_home_div_french_button
    }

    /**
     * Accéder a un utilisateur en mode anonyme.
     *
     * code 302 found
     *
     * rediriger vers la page d'accueil
     */
    public function testGetOneUserAnonymous(): void
    {
        /** @var ContainerInterface */
        $containerInterface = $this->client->getContainer();
        /** @var UrlGeneratorInterface */
        $urlGeneratorInterface = $containerInterface->get('router');

        $this->client->request(
            Request::METHOD_GET,
            $urlGeneratorInterface->generate('user_one', ['id' => 2]) // '/user/2'
        );

        self::assertSame(Response::HTTP_FOUND, $this->client->getResponse()->getStatusCode());
        self::assertResponseRedirects($urlGeneratorInterface->generate('storage_space_all'));
    }

    /**
     * On test la page login lorsqu'un utilisateur se connecte avec des fausses données
     * Il doit être rediriger vers la page /login
     * On suit la redirection
     * On verifie que la classe .error_form_login est bien présente afin d'afficher une erreur à l'utilisateur.
     */
    public function testUserLoginBad(): void
    {
        /** @var ContainerInterface */
        $containerInterface = $this->client->getContainer();
        /** @var UrlGeneratorInterface */
        $urlGeneratorInterface = $containerInterface->get('router');

        $crawler = $this->client->request(
            Request::METHOD_GET,
            $urlGeneratorInterface->generate('app_login') // '/login',
        );

        $form = $crawler->selectButton('Se connecter')->form([
            'email' => AppFixtures::DEFAULT_USER['email'],
            'password' => 'fakePassword',
        ]);

        $this->client->submit($form);

        self::assertResponseRedirects($urlGeneratorInterface->generate('app_login'));
        $this->client->followRedirect();
        self::assertSelectorExists('.error_form_login');
    }

    /**
     * On test la page login lorsqu'un utilisateur se connecte avec des données valide
     * Il doit être rediriger vers la page /storageSpace.
     */
    public function testUserLoginSuccess(): void
    {
        /** @var ContainerInterface */
        $containerInterface = $this->client->getContainer();
        /** @var UrlGeneratorInterface */
        $urlGeneratorInterface = $containerInterface->get('router');

        $crawler = $this->client->request(
            Request::METHOD_GET,
            $urlGeneratorInterface->generate('app_login') // '/login',
        );

        $form = $crawler->selectButton('Se connecter')->form([
            'email' => AppFixtures::DEFAULT_USER['email'],
            'password' => AppFixtures::DEFAULT_USER['password'],
        ]);

        $this->client->submit($form);

        self::assertResponseRedirects($urlGeneratorInterface->generate('storage_space_all'));
    }

    /**
     * On test le contrôleur SecurityController::login
     * lorsqu'un utilisateur se connecte avec des données valide.
     *
     * On récupère le token CSRF du formulaire et on le passe en paramètre avec le email et le password
     * on doit être rediriger vers la page /storageSpace
     */
    public function testSecurityControllerLoginSuccess(): void
    {
        /** @var ContainerInterface */
        $containerInterface = $this->client->getContainer();
        /** @var UrlGeneratorInterface */
        $urlGeneratorInterface = $containerInterface->get('router');

        /** @var CsrfTokenManager */
        $tokenManager = $containerInterface->get('security.csrf.token_manager');

        $csrfToken = $tokenManager->getToken('authenticate');

        $this->client->request(
            Request::METHOD_POST,
            $urlGeneratorInterface->generate('app_login'), // '/login',
            [
                'email' => AppFixtures::DEFAULT_USER['email'],
                'password' => AppFixtures::DEFAULT_USER['password'],
                '_csrf_token' => $csrfToken,
            ]
        );

        self::assertResponseRedirects($urlGeneratorInterface->generate('storage_space_all'));
    }

    /**
     * L'utilisateur connecter accède à son profil
     * On vérifie si le status code est 200 pour la page /user/{id}.
     */
    public function testGetOneUserWithUserConnected(): void
    {
        /** @var ContainerInterface */
        $containerInterface = $this->client->getContainer();
        /** @var UrlGeneratorInterface */
        $urlGeneratorInterface = $containerInterface->get('router');

        $user = $this->createUser();
        // $this->userLogin($this->client, $user);

        $client = $this->createAuthentificationClientUserNormal($this->client, true);

        $client->request(
            Request::METHOD_GET,
            $urlGeneratorInterface->generate('user_one', ['id' => $user->getId()]) // "/user/{$user->getId()}"
        );

        self::assertResponseStatusCodeSame(Response::HTTP_OK);
    }

    /**
     * L'utilisateur en role admin accède à la page /amdin
     * On vérifie si le status code est 200 pour la page /amdin.
     */
    public function testGetAdminDasboardWithAdmin(): void
    {
        /** @var ContainerInterface */
        $containerInterface = $this->client->getContainer();
        /** @var UrlGeneratorInterface */
        $urlGeneratorInterface = $containerInterface->get('router');

        $user = $this->createUserAdmin();

        $this->userLogin($this->client, $user);

        $this->client->request(
            Request::METHOD_GET,
            $urlGeneratorInterface->generate('app_admin') // '/admin'
        );

        self::assertResponseStatusCodeSame(Response::HTTP_OK);
    }

    /**
     * L'utilisateur non admin accède à la page /amdin
     * On vérifie si le status code est 403 pour la page /amdin.
     */
    public function testGetAdminDasboardWithUserNotAdmin(): void
    {
        /** @var ContainerInterface */
        $containerInterface = $this->client->getContainer();
        /** @var UrlGeneratorInterface */
        $urlGeneratorInterface = $containerInterface->get('router');

        $user = $this->createUser();
        // $this->userLogin($this->client, $user);

        $client = $this->createAuthentificationClientUserNormal($this->client, true);

        $client->request(
            Request::METHOD_GET,
            $urlGeneratorInterface->generate('app_admin') // '/admin'
        );

        self::assertResponseStatusCodeSame(Response::HTTP_FORBIDDEN);
    }

    // TOOLS

    /**
     * Créer un utilisateur admin.
     */
    private function createUserAdmin(): User
    {
        $user = new User();
        $user->setId(AppFixtures::DEFAULT_ADMIN['id'])
            ->setEmail(AppFixtures::DEFAULT_ADMIN['email'])
            ->setPassword(AppFixtures::DEFAULT_ADMIN['password_hash'])
            ->setLastName(AppFixtures::DEFAULT_ADMIN['lastName'])
            ->setFirstName(AppFixtures::DEFAULT_ADMIN['firstName'])
            ->setPhoneNumber(AppFixtures::DEFAULT_ADMIN['phoneNumber'])
            // ->setCreatedAt(AppFixtures::DEFAULT_USER['createdAt'])
            ->setRoles(AppFixtures::DEFAULT_ADMIN['roles_admin'])
        ;

        return $user;
    }

    /**
     * Créer un utilisateur.
     */
    private function createUser(): User
    {
        $user = new User();
        $user->setId(AppFixtures::DEFAULT_USER['id'])
            ->setEmail(AppFixtures::DEFAULT_USER['email'])
            ->setPassword(AppFixtures::DEFAULT_USER['password_hash'])
            ->setLastName(AppFixtures::DEFAULT_USER['lastName'])
            ->setFirstName(AppFixtures::DEFAULT_USER['firstName'])
            ->setPhoneNumber(AppFixtures::DEFAULT_USER['phoneNumber'])
            // ->setCreatedAt(AppFixtures::DEFAULT_USER['createdAt'])
            ->setRoles(AppFixtures::DEFAULT_USER['roles_user'])
        ;

        return $user;
    }

    /**
     * L' utilisateur se conntecte.
     *
     * On crée une session pour un utilisateur
     * puis on fait un cookie qui est lier à la session
     */
    private function userLogin(KernelBrowser $client, User $user): void
    {
        // Creer une session pour un utilisateur
        /** @var ContainerInterface */
        // $containerInterface = $this->client->getContainer();
        $containerInterface = static::getContainer();

        /** @var SessionFactory */
        $session = $containerInterface->get('session.factory');

        /** @var SessionInterface */
        $session = $session->createSession(); // acceder au service de session

        // $tokenGenerator = $containerInterface->get('security.csrf.token_generator');
        // $csrfToken = $tokenGenerator->generateToken();
        $token = new UsernamePasswordToken($user, 'main', $user->getRoles()); // généré le token
        $session->set('_security_main', serialize($token)); // firewall main
        $session->save();

        // faire un cookie qui est lier à la session
        $cookie = new Cookie($session->getName(), $session->getId());
        $client->getCookieJar()->set($cookie);
    }

    // protected function setLoginSessionValue(string $name, $value): self
    // {
    //     if (isset($this->session)) {
    //         $this->session->set($name, $value);
    //         $this->session->save();

    //         return $this;
    //     }
    //     throw new \LogicException('loginUser() must be called to initialize session');
    // }

    // public function generateCsrfToken(KernelBrowser $client, string $tokenId): string
    // {
    //     $session = $this->getSession($client);

    //     /** @var ContainerInterface */
    //     $container = static::getContainer();
    //     $tokenGenerator = $container->get('security.csrf.token_generator');
    //     $csrfToken = $tokenGenerator->generateToken();
    //     $session->set(SessionTokenStorage::SESSION_NAMESPACE . "/{$tokenId}", $csrfToken);
    //     $session->save();

    //     return $csrfToken;
    // }

    // public function getSession(KernelBrowser $client): Session
    // {
    //     $cookie = $client->getCookieJar()->get('MOCKSESSID');

    //     // create a new session object
    //     /** @var ContainerInterface */
    //     $container = static::getContainer();
    //     /** @var SessionInterface */
    //     $session = $container->get('session.factory')->createSession();

    //     if ($cookie) {
    //         // get the session id from the session cookie if it exists
    //         $session->setId($cookie->getValue());
    //         $session->start();
    //     } else {
    //         // or create a new session id and a session cookie
    //         $session->start();
    //         $session->save();

    //         $sessionCookie = new Cookie(
    //             $session->getName(),
    //             $session->getId(),
    //             null,
    //             null,
    //             'localhost',
    //         );
    //         $client->getCookieJar()->set($sessionCookie);
    //     }

    //     return $session;
    // }

    /**
     * @phpstan-ignore-next-line
     *
     * généré un email aléatoire
     */
    private function getPayload(): string
    {
        $faker = Factory::create();

        return sprintf($this->userPayload, $faker->email);
    }
}
